requirejs.config({

    baseUrl: 'js',

    paths: {

        jquery: [
        	'//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min',
        	'jquery.min'
        ],
        placeholders: 'placeholders.min',
        bootstrap: 'bootstrap.min',
        bootstrapTab: 'bootstrapTab',
        bootstrapTransition: 'bootstrapTransition',
        bootstrapDropdown: 'bootstrapDropdown'
    }

});



requirejs(['jquery', 'placeholders', 'bootstrap', 'bootstraptab', 'bootstrapTransition', 'bootstrapDropdown'],
function ($,placeholders,bootstrap) {
	Placeholders.init();
	$('#share a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});
	$('.dropdown-toggle').dropdown();
});

